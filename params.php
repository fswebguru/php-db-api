<?php

$params = [
    [
        'slug' => 'session',
        'table' => 'session',
        'columns' =>  [
            'sessionId',
            'userId',
            'sessionComplete',
            'sessionProductId',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ],
    [
        'slug' => 'user',
        'table' => 'userDetails',
        'columns' =>  [
            'userId',
            'userFirstName',
            'userLastName',
            'userImagePath',
            'userJobTitle',
            'userAddressLine1',
            'userAddressCity',
            'userAddressCountry',
            'userContactNumber',
            'userContactEmail',
            'userContactWebsite',
            'userReferencesOnRequest',
            'userType',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ],
    [
        'slug' => 'education',
        'table' => 'education',
        'columns' =>  [
            'educationId',
            'sessionId',
            'educationLevel',
            'educationStatus',
            'educationSchool',
            'educationLocation',
            'educationCourseTitle',
            'educationQualification',
            'educationstartYear',
            'educationEndYear',
            'educationSummary',
            'priorityValue',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ],
    [
        'slug' => 'employment',
        'table' => 'employment',
        'columns' =>  [
            'employmentId',
            'sessionId',
            'employmentCompany',
            'employmentCountry',
            'employmentLocation',
            'employmentTitle',
            'employmentStartDate',
            'employmentEndDate',
            'employmentisPresent',
            'employmentSummary',
            'priorityValue',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ],
    [
        'slug' => 'profile',
        'table' => 'profile',
        'columns' =>  [
            'profileId',
            'sessionId',
            'profileSummary',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ],
    [
        'slug' => 'reference',
        'table' => 'reference',
        'columns' =>  [
            'referenceId',
            'sessionId',
            'referenceName',
            'referenceCompany',
            'referenceTitle',
            'referenceContactNumber',
            'referenceEmail',
            'priorityValue',
            'dateTimeModified',
            'dateTimeCreated'
        ]
	],
    [
        'slug' => 'skillsProfessional',
        'table' => 'skillsProfessional',
        'columns' =>  [
            'skillsProfessionalId',
            'sessionId',
            'skillsProfessionalTitle',
            'skillsProfessionalScore',
            'priorityValue',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ],
    [
        'slug' => 'skillsPersonal',
        'table' => 'skillsPersonal',
        'columns' =>  [
            'skillsPersonalId',
            'sessionId',
            'skillsPersonalTitle',
            'skillsPersonalScore',
            'priorityValue',
            'dateTimeModified',
            'dateTimeCreated',
            'userContactNumber',
            'userContactEmail',
            'userContactWebsite',
            'userReferencesOnRequest',
            'userType',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ],
    [
        'slug' => 'social',
        'table' => 'social',
        'columns' =>  [
            'socialId',
            'sessionId',
            'socialName',
            'socialValue',
            'socialStatus',
            'priorityValue',
            'dateTimeModified',
            'dateTimeCreated'
        ]
    ]
];

