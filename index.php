<?php

include dirname( __FILE__ ) . '/includes/compatibility.php';
include dirname( __FILE__ ) . '/includes/functions.php';
include dirname( __FILE__ ) . '/includes/class.db-api.php';
include dirname( __FILE__ ) . '/config.php';
include dirname( __FILE__ ) . '/params.php';

$query = $db_api->parse_query();
$db_api->set_db( $args['name'] );

$allResults = [];
foreach ($params as $param) {
    $allResults[$param['slug']] = $db_api->query( array_merge([
        'table' => $param['table']
    ], $query), $param['columns'] );
}

$renderer = 'render_json';
$db_api->$renderer( $allResults, $query );
