Database to API
=======================

General RESTful APIs from the contents of a database table to return JSON data.


Database Supported
-------------------

* MySQL

Usage
-----

1. Copy `config.sample.php` to `config.php`
2. Edit `config.php` to include the database credentials:

```php

$args = array( 
			'name' => 'database_name',
			'username' => 'username',
			'password' => 'password',
			'server' => 'localhost',
			'port' => 3306,
			'type' => 'mysql',
			'table_blacklist' => array(),
			'column_blacklist' => array(),
);

register_db_api( $args );

```

API Structure
-------------

* API endpoint: `/resume-fetch.php`

Required Parameters
---------------------

* `action`: String (Fixed Value for this parameter should be: “gather”)
* `userId`: Integer
* `sessionId`: Integer

Additional Parameters
---------------------

* `order_by`: name of column to sort by
* `direction`: direction to sort, either `asc` or `desc` (default `asc`)
* `limit`: number, maximum number of results to return
